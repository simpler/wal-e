import json
import logging
import logging.config
import os
import time


class WalELogger(logging.getLoggerClass()):
    def log(self, level, msg, *args, **kwargs):
        kwargs['extra'] = {
            'detail': kwargs.pop('detail', None),
            'hint':  kwargs.pop('hint', None),
        }
        super(WalELogger, self).log(level, msg, *args, **kwargs)

    # Boilerplate convenience shims to different logging levels.  One
    # could abuse dynamism to generate these bindings in a loop, but
    # one day I hope to run with PyPy and tricks like that tend to
    # lobotomize an optimizer something fierce.

    def debug(self, *args, **kwargs):
        self.log(logging.DEBUG, *args, **kwargs)

    def info(self, *args, **kwargs):
        self.log(logging.INFO, *args, **kwargs)

    def warning(self, *args, **kwargs):
        self.log(logging.WARNING, *args, **kwargs)

    def error(self, *args, **kwargs):
        self.log(logging.ERROR, *args, **kwargs)

    def critical(self, *args, **kwargs):
        self.log(logging.CRITICAL, *args, **kwargs)


logging.setLoggerClass(WalELogger)


class UTCFormatter(logging.Formatter):
    # Undocumented, seemingly still in 2.7 (see
    # http://od-eon.com/blogs/stefan/logging-utc-timestamps-python/)
    converter = time.gmtime


class DefaultWalEFormatter(UTCFormatter):
    def format(self, record, *args, **kwargs):
        """
        Format a message in the log

        Act like the normal format, but indent anything that is a
        newline within the message.

        """
        return super(DefaultWalEFormatter, self).format(
            record, *args, **kwargs).replace('\n', '\n' + ' ' * 8)


def utc_formatter_factory(*args, **kwargs):
    return UTCFormatter(*args, **kwargs)


def default_wale_formatter_factory(*args, **kwargs):
    return DefaultWalEFormatter(*args, **kwargs)


def configure(dict_config_path=None):

    if dict_config_path is not None:
        with open(dict_config_path) as fd:
            logging.config.dictConfig(fd)
    elif 'WALE_LOGGING_CONFIG' in os.environ:
        with open(os.environ['WALE_LOGGING_CONFIG']) as fd:
            logging.config.dictConfig(json.load(fd))
    else:
        if os.path.exists('/dev/log'):
            default_syslog_address = '/dev/log'
        else:
            # try UDP (e.g. osx)
            default_syslog_address = ('localhost', 514)

        default = {
            'version': 1,
            'formatters': {
                'wale_formatter': {
                    '()': 'wal_e.log.default_wale_formatter_factory',
                    'datefmt': "%Y-%m-%dT%H:%M:%S",
                    # Date and format modified to produce a more standard
                    # ISO8601 millisecond-including timestamp.  At the very
                    # least, it was chosen to very carefully be parsable with
                    # PostgreSQL's timestamptz datatype.  It also avoids the
                    # representation of ISO8601 with spaces.
                    'format': ('%(asctime)s.%(msecs)03d+00 '
                               'pid=%(process)d '
                               '%(name)-12s '
                               '%(levelname)-8s '
                               'MSG: %(message)s\n'
                               'DETAIL: %(detail)s\n'
                               'HINT: %(hint)s\n'),
                },
            },
            'handlers': {
                'wale_handler': {
                    'class': 'logging.handlers.SysLogHandler',
                    'formatter': 'wale_formatter',
                    'address': default_syslog_address,
                    'level': 'INFO',
                },
            },
            'loggers': {
                'wal_e': {
                    'level': 'INFO',
                    'handlers': ['wale_handler'],
                },
            },
        }
        try:
            logging.config.dictConfig(default)
        except ValueError:
            # Silence on configuration failure.
            pass
        logging.config.dictConfig(default)
